#!/bin/bash

_git_cur_branch=$(git branch --show-current)
_git_cmd_files_modified='git status|grep -Po "modified:\x20+\K\S+"'
_git_cmd_files_new='git status|grep -Po "new file:\x20+\K\S+"'
export LANGUAGE="en_US.UTF-8"

#directories=$(find . -maxdepth 1 -name "*"  ! -path "."  ! -path "./.git" -type d -ls|awk '{print $11}'|sed 's|./||g')
#git add ${directories}
git add .

_git_status=$(git status)

_git_cur_branch=$(git branch --show-current)
_git_files_modified=$(eval "${_git_cmd_files_modified}")
_git_files_new=$(eval "${_git_cmd_files_new}")

message="modified files: ${_git_files_modified}; new files: ${_git_files_new}"

if [ -n "${1}" ]; then message="${1}"; fi

git commit -m "${message}"
git fetch origin ${_git_cur_branch}
git diff origin/${_git_cur_branch}

git pull origin ${_git_cur_branch}
git push origin ${_git_cur_branch}
